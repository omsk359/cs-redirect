const Koa = require('koa');
const app = new Koa();

const koaBody = require('koa-body');
const apiRouter = require('./routes');

app.use(koaBody());
app.use(apiRouter.routes());

app.listen(3000);