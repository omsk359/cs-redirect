const Router = require('koa-router');
const redis = require('./redis');

const router = new Router();

router.post('/set-url', async (ctx, next) => {
	let { src, dst } = ctx.request.body;
	console.log('POST ctx.request', src, dst);

	if (src.startsWith('http') || src.startsWith('/')) {
		await redis.set(src, dst);
		ctx.body = { ok: true };
	} else {
		ctx.body = { ok: false };
	}
});

router.get('/r/:src', async (ctx, next) => {
	console.log('GET ctx.params', ctx.params);

	let { src } = ctx.params;
	if (!src.startsWith('http')) {
		src = `/${src}`;
	}
	let dst = await redis.get(src);

	console.log('src', src, 'dst', dst);

	if (dst) {
		return ctx.redirect(dst);
	}

	next();
});

module.exports = router;
